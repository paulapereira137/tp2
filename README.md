﻿# Trabajo Práctico 2

# Métodos computacionales en ingeniería 1
## Información sobre distintos temas
### Primera parte
En esta parte del trabajo se describe que son y como se usan Git y Markdown.

***
**Git**: Es un controlador de versiones.
***
**Markdown**: Es un lenguaje de marcado.
***
### Segunda Parte
En esta parte se presentará un grafico con informacion sobre distintas partes de una CPU.

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVERcbkEoVW5pZGFkIENlbnRyYWwgZGUgUHJvY2VzYW1pZW50bykgXG5BIC0tPkJbQ1BVXSBcbkIgLS0-Q1tBTFVdXG5CIC0tPkRbVW5pZGFkIGRlIGNvbnRyb2xdXG5CIC0tPkVbUmVnaXN0cm9zXVxuQyAtLT58VW5pZGFkIEFyaXRtZXRpY29sw7NnaWNhfEZbVW5pZGFkXVxuRCAtLT5HW0J1c2NhIGluc3RydWNjaW9uZXNdXG5FIC0tPkhbTWVtb3JpYSBwZXF1ZcOxYSB5IHLDoXBpZGFdXG4iLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcbkEoVW5pZGFkIENlbnRyYWwgZGUgUHJvY2VzYW1pZW50bykgXG5BIC0tPkJbQ1BVXSBcbkIgLS0-Q1tBTFVdXG5CIC0tPkRbVW5pZGFkIGRlIGNvbnRyb2xdXG5CIC0tPkVbUmVnaXN0cm9zXVxuQyAtLT58VW5pZGFkIEFyaXRtZXRpY29sw7NnaWNhfEZbVW5pZGFkXVxuRCAtLT5HW0J1c2NhIGluc3RydWNjaW9uZXNdXG5FIC0tPkhbTWVtb3JpYSBwZXF1ZcOxYSB5IHLDoXBpZGFdXG4iLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)