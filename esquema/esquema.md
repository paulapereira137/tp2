﻿
<script src="mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

```mermaid
sequenceDiagram
master -->> +master: git branch tema1
master -->> +tema1: git checkout tema1
tema1 -->> +tema1: se crea: carpeta tema 1 y tema1.md
tema1 -->> +master: git checkout master
master -->> +tema1: git checkout tema1
tema1-->> +tema1: cambio en tema1.md
tema1 -->> +master: git checkout master
master --> +tema1: git merge tema1
```
