﻿# Práctica 2: Markdown y herramientas de programación colaborativas

## Parte 1: Git y Markdown

### Git

#### Comprensión

1.b. Complete con las definiciones que encuentre o con lo que entiende qué son los siguientes conceptos:

1. **Software de control de versiones.**
El propósito de un software de control de versiones, es llevar registro de los cambios en un archivo de computadora y coordinar el trabajo que varias personas realizan sobre archivos compartidos. Ayuda a organizar el código de trabajo, el historial y la evolución de un trabajo, funciona como una maquina del tiempo que te permite navegar por las diferentes versiones de un mismo proyecto.

2. **Repositorios. ¿Qué significa que sean públicos o privados?**
Un repositorio o directorio de trabajo, es una carpeta principal donde se encuentran almacenados los archivos que componen un proyecto.
Cada proyecto tiene un nivel de visibilidad, que controla quien tiene acceso de lectura a las paginas del proyecto y al propio repositorio. Si es privado, el propietario debe compartir los accesos para conceder los permisos a determinados usuarios. Si es público es visible a todos, incluso usuarios no identificados y visitantes.

3. **Árboles de Merkle y su uso en Git.**
Según bit2me “Un árbol merkle, es una estructura de datos divida en varias capas que tiene como finalidad relacionar cada nodo con una raíz única asociada a los mismos.”. Este sistema en Git permite ir a un commit especifico para ver cual era el estado del código del proyecto en un momento determinado, mientras el estado actual del proyecto permanece intacto.

4. **Copia local y copia remota de los datos.**
La copia local, es la que se guarda en el propio equipo, mientras que la copia remota es la que esta almacenada en un servidor externo.

5. **¿Qué es un commit y cuándo deberías hacerlo?**
Un commit es el estado del proyecto en un determinado momento de su historia. Usualmente, cada commit va acompañado de un hash que lo identifica y un mensaje que describe los cambios que contiene. Lo ideal es hacerlos con frecuencia, cada vez que se ejecuta un cambio, para que quede registro del mismo.

6. **¿Cómo volverías a una versión anterior de tu trabajo si te das cuenta que gran parte de lo que avanzaste debe ser modificado?**
Para reemplazar los cambios locales en tu directorio de trabajo por el ultimo contenido de head, se puede utilizar el comando: git checkout \-- <\filename>
En cambio, si se quieren deshacer todos los cambios locales y commits, se puede volver a la ultima versión del servidor con los comandos: git fetch origin
git reset \--hard origin/master

7. **¿Qué son los branches (ramas) y para que se usan?**
La ramas se utilizan para trabajar en una nueva funcionalidad mientras se deja intacta la versión principal del proyecto. Es como un ambiente de trabajo paralelo a la versión original, que permite trabajar sin afectarla.

##### Fuentes ademas de la sugeridas:

[Git en el servidor GitLab](https://git-scm.com/book/es/v2/Git-en-el-Servidor-GitLab)  
[Que es un arbol de Merkle](https://academy.bit2me.com/que-es-un-arbol-merkle/)  
[Conceptos de master, origin y head](https://www.campusmvp.es/recursos/post/git-los-conceptos-de-master-origin-y-head.aspx )  

### Markdown

#### Comprensión

2.b. Responder:

1. **¿Qué es el lenguaje Markdown?**
Markdown es un lenguaje de marcado que permite aplicar formato a un texto usando caracteres de forma especial.
Según John Gruber, unos de sus creadores, “Markdown es realmente dos cosas: por un lado, el lenguaje; por otro, una herramienta de software que convierte el lenguaje en HTML válido”

2. **¿Para que se lo utiliza y cuales son las ventajas de este lenguaje?**
Inicialmente, este lenguaje fue pensado para escribir textos cuyo destino era la web de manera mas rápida, sencilla y sin la necesidad de usar directamente HTML. Ese sigue siendo su uso principal, pero es posible emplearlo en cualquier tipo de texto, independientemente de cual sea su destino.
Entre sus ventajas se destacan las siguientes: hace que escribir para la web sea más rápido y cómodo, su sintaxis es mas fácil de leer que en HTML, hace que sea mas difícil cometer errores que en HTML, es perfecto para usar con editores de texto minimalistas y es cómodo de usar en dispositivos móviles táctiles.