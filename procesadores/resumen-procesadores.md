# Organizaci�n de una computadora tipo Von Neumann

Las computadoras actuales b�sicas son dise�adas seg�n la arquitectura Von Neumann, est�n compuestas por la unidad central de procesamiento, memorias y dispositivos de entrada y salida.

La unidad central de procesamiento, a la que com�nmente nos referimos por su sigla CPU, tiene la funci�n de ejecutar los programas que est�n guardados en la memoria. Procede buscando las instrucciones del programa que se quiera ejecutar, las examina y luego las ejecuta una tras otra.

Las partes b�sicas de una CPU son: la unidad de control, la ALU y los registros internos. La unidad de control se encarga de buscar instrucciones en la memoria principal y de determinar su tipo.

ALU es la sigla de unidad de aritm�tica y l�gica en ingles, su funci�n es realizar operaciones matem�ticas.

Por ultimo los registros, son memorias peque�as y de alta velocidad que se encuentran en la CPU. Sirven para almacenar informaci�n temporalmente. Los dos registros mas importantes son el contador de programas (PC) y el registro de instrucciones (IR), el primero apunta a la siguiente instrucci�n que se debe buscar y ejecutar, el segundo contiene la instrucci�n que se est� ejecutando.

Las partes de la CPU interact�an entre si y forman parte de lo que se llama camino de datos. Los registros alimentan los registros de entrada de la ALU, estos contienen la entrada de la ALU mientras �sta est� calculando. La ALU suma, resta y realiza otras operaciones simples con sus entradas y produce un resultado en el registro de salida. Finalmente, este registro de salida se envi� a un registro, que si se desea puede escribirse despu�s en la memoria.

Pasando a otra parte de la computadora, la memoria es el lugar en el que se almacenan programas y datos. Las computadoras ejecutan la mayor�a de los programas desde la memoria principal, a la cual la CPU accede directamente, por eso tambi�n es llamada memoria de acceso inmediato. Esta memoria es vol�til (necesita energ�a para mantener la informaci�n) y regrabable (puede ser modificada).Dado que la memoria principal suele ser demasiado peque�a para todos los programas y datos que se necesita almacenar y es vol�til, la mayor�a de los sistemas inform�ticos incluyen un almacenamiento secundario, este puede guardar grandes cantidades de informaci�n de forma permanente. Los dispositivos almacenamiento secundario mas comunes son los discos duros y los dispositivos de memoria no vol�til.

Los dispositivos perif�ricos de entrada y salida (dispositivos de E/S) permiten la interacci�n con el usuario. Las dos tareas principales de una computadora son la E/S y la computaci�n, pero en muchos casos el trabajo que mas realiza es el de E/S. Los dispositivos de E/S var�an mucho en su funci�n y velocidad, por lo que se necesitan diversos m�todos para controlarlos:  
* Los controladores de dispositivos son circuitos que manejan el flujo de informaci�n entre el procesador y el dispositivo de E/S. Estos junto con otros elementos b�sicos de hardware, como los puertos, pueden ser compatibles con una gran una gran variedad de dispositivos.  
* Para encapsular las rarezas y los detalles de cada uno de los dispositivos, existen los driver de dispositivos, que comprenden al controlador del dispositivo y proporcionan al sistema operativo una interfaz uniforme para el dispositivo.

Los diversos componentes que forman parte de una computadora simple, por lo general se comunican entre si mediante un bus de datos. Tambi�n las partes de la CPU se comunican entre ellas por medio de uno de estos. Un bus, es un conjunto de cables y un protocolo definido que especifica el conjunto de mensajes que puede enviarse a trav�s de los cables. En t�rminos de electr�nica, los mensajes se trasmiten seg�n patrones de voltaje el�ctrico aplicados a los cables en tiempos definidos.

Una parte clave de la interacci�n del sistema operativo y el hardware son las interrupciones. El hardware puede activar una interrupci�n enviando una se�al a la CPU por medio del bus del sistema. Los controladores de dispositivos y las fallas de hardware generan interrupciones. Cuando la CPU recibe una interrupci�n, detiene inmediatamente lo que est� haciendo y se ejecuta una rutina de servicio para la interrupci�n, cuando esta termina, la CPU reanuda la operaci�n interrumpida.

Bibliograf�a: Lecturas te�ricas de la materia. 
* Tanenbaum A. S. (2000) *Organizaci�n de computadora un enfoque estructurado* Amsterdam, Paises Bajos: Editorial Pearson Education.  
* Silberschatz A., Galvin P. B. y Gagne G. (2018) *Operating system concepts* Estados Unidos: Editorial Wiley.  
* Tenenbaum A. S. y Austin T. (2013) *Structure computer organizat�on* Estados Unidos: Editorial Pearson.



